import random

grafica = {
            1:[2,3,4,5],
            2:[1,4,6,7],
            3:[1,4,5],
            4:[1,2,3,5,6,7,8],
            5:[1,3,4,7,6],
            6:[5,4,2,7,8],
            7:[2,4,5,6,8],
            8:[6,4,7],
        }

# auxiliares:

def listaAristas(g):
    lista =[]
    visitados=[]
    for i in g:
        visitados.append(i)
        lista = lista + [(i,x) for x in list(set(g.get(i))-set(visitados))]
    return lista

# verifica si alguno de los nodos de la cubierta de vertice
# es uno que este conectado en esa arista
def esta(cubiertaDeVertices ,arista): 
     for nodo in cubiertaDeVertices:
             if(nodo in arista):
                     return True
     return False

###########

#adivinadora
def  cubierta_v(g,k): # recibe la grafica y un numero k de la cubierta de vertices
    aristas = len(listaAristas(g))
    vertices = [x for x in grafica]
    cubierta = []
    #n = random.randint(1,vertices)
    for i in range(k-1):
        cubierta.append( vertices.pop(   random.randint( 0 , len(vertices)-1 )   )) 
    return cubierta

#verificadora
def verifica(cv,g):
    aristas = listaAristas(g)
    for i in aristas:
        if(esta(cv,i)):
            pass
        else:
            return False
    return True

if __name__ == "__main__":
    print("Grafica: \n",grafica)
    print("cubierta de vertices con k = 5")
    cv = cubierta_v(grafica,6)
    print(cv,"\n")
    print("verifica la cubierta...")
    print(verifica(cv,grafica))


